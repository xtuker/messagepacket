//**************************************************************************/
// DESCRIPTION: MessagePacket. Class to work with messages format of key-value
// AUTHOR: Falkovski Alexander
// (C)2014
//***************************************************************************/

#pragma once
#include <string>
#include <map>

typedef std::multimap<std::string, std::string> ParamList;

class PacketHeader
{
public:
    enum PacketType{AnyPacket, BadPacket};
protected:
    static const char EndOfHeader = 0x1D;
    static const char EndOfValue = 0x1E;
    static const char EndOfParam = 0x1F;
    static const unsigned int HandleSize = 32;
    
    char Handle[HandleSize];
    PacketType Type;
    const char EOH;
public:
    static const unsigned int HeaderSize = HandleSize + sizeof(PacketType) + sizeof(char);
    const void * Header;
    PacketHeader() : Header(Handle), EOH(EndOfHeader)
    {
        memset(Handle, 0, HandleSize);
        Type = AnyPacket;
    }
    PacketHeader(std::string const &handle, PacketType t = AnyPacket) : Header(Handle), EOH(EndOfHeader)
    {
        SetHandle(handle);
        SetType(t);
    }
    virtual ~PacketHeader() {}
    void SetHandle(std::string const &handle)
    {
        unsigned int length = handle.size() > HandleSize ? HandleSize : handle.size();
        memset(Handle, 0, HandleSize);
        memcpy_s(Handle, HandleSize, handle.data(), length);
    }
    void SetType(PacketType t)
    {
        if(t < AnyPacket || t > BadPacket)
            Type = BadPacket;
        else
            Type = t;
    }
    bool operator==(PacketHeader const &header) const
    {
        if(Type != header.Type)
            return false;
        if(EOH != header.EOH)
            return false;
        for(int i = 0; i < HandleSize; i++)
            if(Handle[i] != header.Handle[i])
                return false;
        return true;
    }
};

class MessagePacket : public PacketHeader
{
private:
    ParamList Params;
    std::string IpAddress;

    void ParseHeader(std::string const &);
    void ParseParams(std::string const &);
    void FillPacket(std::string const &);
public:
    MessagePacket(void);
    MessagePacket(std::string const &data);
    MessagePacket(std::string const &handle, PacketType t);
    ~MessagePacket(void);

    bool AddParam(std::string const &param, std::string const &value);
    bool AddParam(std::string const &value);
    void ClearParams();

    void Clear();

    std::string operator[](std::string const &param) const;
    std::string operator[](size_t nparam) const;

    MessagePacket& operator=(std::string const &data);
    MessagePacket& operator=(MessagePacket const &);

    void SetIpAddress(std::string const &ipaddr) { IpAddress = ipaddr; }
    std::string GetPrintData() const;

    std::string GetHandle() const { return std::string(Handle, &Handle[HandleSize]); }
    PacketType GetType() const { return Type; }
    std::string GetIpAddress() const { return IpAddress; }

    ParamList const * GetParams() const { return &Params; }
};

